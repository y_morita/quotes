import Pagination from "../../components/pagination";
import Card from "../../components/card";
import Layout from "../../components/layout";
import { client } from "../../libs/client";

const PER_PAGE = 15;

// pages/blog/[id].js
export default function BlogPageId({ posts, totalCount, tags }) {
  return (
    <Layout tags={tags}>
      <div className="main">
        <ul>
          {posts.map((post) => (
            <li key={post.id}>
              <Card
                title={post.title}
                tag={post.tag}
                contents={post.contents}
                date={post.createdAt}
                category={post.category}
              />
            </li>
          ))}
        </ul>
        <Pagination totalCount={totalCount} />
      </div>
    </Layout>
  );
}

// 動的なページを作成
export const getStaticPaths = async (context) => {
  const key = {
    headers: { "X-API-KEY": process.env.API_KEY },
  };

  const res = await fetch("https://laprn.microcms.io/api/v1/quotes", key);

  const repos = await res.json();

  const pageNumbers = [];

  const range = (start, end) =>
    [...Array(end - start + 1)].map((_, i) => start + i);

  const paths = range(1, Math.ceil(repos.totalCount / PER_PAGE)).map(
    (repo) => `/page/${repo}`
  );

  return { paths, fallback: false };
};

// データを取得
export const getStaticProps = async (context) => {
  const id = context.params.id;
  const key = {
    headers: { "X-API-KEY": process.env.API_KEY },
  };

  const data = await fetch(
    `https://laprn.microcms.io/api/v1/quotes?offset=${
      (id - 1) * PER_PAGE
    }&limit=${PER_PAGE}`,
    key
  )
    .then((res) => res.json())
    .catch(() => null);

  const data2 = await client.get({
    endpoint: "quotes",
    queries: { limit: 1024 },
  });
  const tags = data2.contents
    .map((element) => element.tag)
    .filter(function (x, i, self) {
      return self.indexOf(x) == i;
    });
  return {
    props: {
      posts: data.contents,
      totalCount: data.totalCount,
      tags: tags,
    },
  };
};
