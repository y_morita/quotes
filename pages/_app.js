import '../styles/global.css'
import localFont from '@next/font/local'

const myFont = localFont({ src: './IBMPlexSansJP-Regular.otf' })

function App({ Component, pageProps }) {
    return (
        <main className={myFont.className}>
            <Component {...pageProps} />
        </main>
    )
}

export default App