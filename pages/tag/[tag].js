// pages/blog/page/[id].js
import Card from "../../components/card";
import Layout from "../../components/layout";
import { client } from "../../libs/client";

const LIMIT = 1024;
const key = {
  headers: { "X-API-KEY": process.env.API_KEY },
};

// pages/tag/[id].js
export default function BlogPageId({ posts, tags, all_tags }) {
  return (
    <Layout tags={all_tags}>
      <div className="main">
        <ul>
          {posts.map((post) => (
            <li key={post.id}>
              <Card
                title={post.title}
                tag={post.tag}
                contents={post.contents}
                date={post.createdAt}
                category={post.category}
              />
            </li>
          ))}
        </ul>
      </div>
    </Layout>
  );
}

// 動的なページを作成
export const getStaticPaths = async (context) => {
  const data = await fetch(
    `https://laprn.microcms.io/api/v1/quotes?limit=${LIMIT}`,
    key
  )
    .then((res) => res.json())
    .catch(() => null);

  const paths = data.contents.map((content) => `/tag/${content.tag}`);
  console.log(paths);
  return { paths, fallback: false };
};

// データを取得
export const getStaticProps = async (context) => {
  const tag = context.params.tag;
  console.log("Tag", tag);
  const data = await client.get({
    endpoint: "quotes",
    queries: { filters: `tag[contains]${tag}`, limit: LIMIT },
  });
  const data2 = await client.get({
    endpoint: "quotes",
    queries: { limit: LIMIT },
  });
  const all_tags = data2.contents
    .map((element) => element.tag)
    .filter(function (x, i, self) {
      return self.indexOf(x) == i;
    });
  return {
    props: {
      posts: data.contents,
      all_tags: all_tags,
    },
  };
};
