import { client } from "../libs/client";
import Layout from "../components/layout";
import Card from "../components/card";
import Pagination from "../components/pagination";

export default function Home({ posts, totalCount, tags }) {
  return (
    <Layout tags={tags}>
      <div className="main">
        <ul>
          {posts.map((post) => (
            <li key={post.id}>
              <Card
                title={post.title}
                tag={post.tag}
                contents={post.contents}
                date={post.createdAt}
                category={post.category}
              />
            </li>
          ))}
        </ul>
        <Pagination totalCount={totalCount} />
      </div>
    </Layout>
  );
}

export const getStaticProps = async () => {
  const data = await client.get({
    endpoint: "quotes",
    queries: { limit: 1000 },
  });
  const tags = data.contents
    .map((element) => element.tag)
    .filter(function (x, i, self) {
      return self.indexOf(x) == i;
    });
  return {
    props: {
      posts: data.contents.slice(0, 15),
      totalCount: data.totalCount,
      tags: tags,
    },
  };
};
