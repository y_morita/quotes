import Head from "next/head";
import Link from "next/link";
import Sidebar from "./sidebar";

export default function Layout({ tags, children }) {
  return (
    <div className="layout">
      <Head>
        <title>quotes.cocno.co</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <h1 id="site-title">
        <Link href={"/"}>Quotes</Link>
      </h1>
      <span>小説・映画・ゲームからの一節を記録</span>
      {/* <div className='nav'>
                <ul>
                    <li className='navlist'><Link href={'/'}>Home</Link></li>
                    <li className='navlist'><Link href={'/about'}>About</Link></li>
                </ul>
            </div> */}
      <div className="container">
        <Sidebar tags={tags} />
        {children}
      </div>
    </div>
  );
}
