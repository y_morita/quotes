import Category from "./category";
import Tag from "./tag";

export default function Sidebar({ tags }) {
  return (
    <div className="sidebar">
      <Category />
      <Tag tags={tags} />
    </div>
  );
}
