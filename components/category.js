import Link from 'next/link'

export default function Category() {
    return (
        <div className='category'>
            <h3>Categories</h3>
            <ul>
                <li><Link href='/category/fiction'>Fiction</Link></li>
                <li><Link href='/category/cinema'>Cinema</Link></li>
                <li><Link href='/category/game'>Game</Link></li>
            </ul>
        </div>
    )
}