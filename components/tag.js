import Link from "next/link";

export default function Tag({ tags }) {
  return (
    <div className="tags">
      <h3>Tags</h3>
      {tags.map((tag) => (
        <span key={tag}>
          {" "}
          <Link href={`/tag/${tag}`}> {tag} </Link> |{" "}
        </span>
      ))}
    </div>
  );
}
