import dayjs from 'dayjs'

export default function Card(props) {
    return (
        <div className='card'>
            <h3 className='post-title'>{props.title}</h3>
            <div
                dangerouslySetInnerHTML={{
                    __html: `${props.contents}`,
                }}
            />
            <span className='date'>{dayjs(props.date).format('YYYY年MM月DD日')}</span> | <span className='tag'>{props.category}</span>
        </div>
    )
}