import Router from "next/router";
import Link from "next/link";

function Pagination({ totalCount }) {
  const PER_PAGE = 15;

  const range = (start, end) =>
    [...Array(end - start + 1)].map((_, i) => start + i);

  return (
    <ul>
      {range(1, Math.ceil(totalCount / PER_PAGE)).map((number, index) => (
        <li key={index} className="pagination">
          <Link href={`/page/${number}`}>{number}</Link>
        </li>
      ))}
    </ul>
  );
}

export default Pagination;
